package automation;

public class Application {
    public static void main(String[] args){
        System.out.println(calcStringLength("12345qwerty"));
        System.out.println(calcStringLength(""));

    }

    public static int calcStringLength(String s){
        if (s.length() > 0){
            return s.length();
        } else throw new IllegalArgumentException("Empty string");
    }
}
